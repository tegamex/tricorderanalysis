import os
import tarfile
import re
import difflib
from pprint import pprint
import matplotlib.pyplot as plt
from pprint import pprint
import shutil

def get_overlap(names):
    s1 = None
    for name in names:
        s1 = s1.intersection(name.split("_")) if s1 else set(name.split("_"))
    return s1

def get_files(experiments):
    with tarfile.open(experiments) as f:    
        files     = [re.sub(r'\_\d+\_csv$', '', filename)  for filename in f.getnames()]
    return files

def get_workloads(experiments):
    files = get_files(experiments)
    overlaps = get_overlap(files)
    uniques  = set(map(lambda x:re.sub(r'(\_\|)+', '_|',re.sub("|".join(overlaps),"|",x)).strip("|"),files))
    workloads = set(map(lambda i:i.split("|")[0].strip("_"),uniques))
    return workloads

def get_defects(experiments):
    files    = get_files(experiments)
    overlaps = get_overlap(files)
    uniques  = set(map(lambda x:re.sub(r'(\_\|)+', '_|',re.sub("|".join(overlaps),"|",x)).strip("|"),files))
    possible_defects = set(map(lambda i:(i.split("|")[1] if "|" in i else "").strip("_"),uniques))
    if "" in possible_defects: return list(filter(lambda x:x,possible_defects))

    mintexts = min(map(lambda x:len(x.split("_")),possible_defects))
    overlaps = set(filter(lambda x:len(x.split("_"))==mintexts,possible_defects))
    defects  = set(map(lambda x:re.sub("|".join(overlaps),"",x).strip("_"),possible_defects))
    assert "" in defects
    return list(filter(lambda x:x,defects))

def copy_ref_data(experiments,output,workload_type,defect,defects):
    path = os.path.join(output,workload_type,defect)
    try:
        shutil.rmtree(path)
    except OSError:
        os.remove(path)
    os.makedirs(path)
    with tarfile.open(experiments) as f:
        for filename in f.getnames():
            if not re.search(f"{workload_type}",os.path.basename(filename)) or re.search(f"{'|'.join(defects)}",os.path.basename(filename)):
                continue
            with f.extractfile(filename) as fi,open(os.path.join(path,os.path.basename(filename)),"wb") as fo:
                 fo.write(fi.read())            
    
def detect_anomaly(experiment,output,workload,defect):
    path              = os.path.join(output,workload,defect)
    fig, axs = None,None
    for filename in os.listdir(path):
        df = pd.read_csv(os.path.join(path,filename),sep=";",header=None)
        df.columns = map(lambda x:f"c{x}",df.columns)
        if fig is None:
            fig, axs = plt.subplots(len(df.columns),2,figsize=(25,15))
        for i,col in enumerate(df.columns):
            axs[i][1 if defect in filename else 0].plot(df[col])
            axs[i][1 if defect in filename else 0].set_title(col)
    plt.show()
    return df

def run_experiment(experiment,output,workload,defect):
    experiments = os.path.join(home,f"experiments/{experiment}.tar.gz")
    with tarfile.open(experiments) as f:
        files = [filename for filename in f.getnames() if re.search(f"{workload}.*{defect}",os.path.basename(filename))]
    
    files = sorted(files,key=lambda file:int(file.split("_")[-2]))
    
    path              = os.path.join(output,workload,defect)
    first_found       = -1
    restrictive_found = -1
    for i,filename in enumerate(files):
        with tarfile.open(experiments) as f:
            with f.extractfile(filename) as fi,open(os.path.join(path,os.path.basename(filename)),"wb") as fo:
                 fo.write(fi.read())
        print(f"Analyzing {i+1} from {len(files)}",filename)
        
        anomaly_found = detect_anomaly(experiment,output,workload,defect)
        
        if anomaly_found and first_found == -1:
            first_found = i+1
            print(f"Detected anomaly in {first_found} iteration")
        
        if anomaly_found and restrictive_found == i:
            print(f"Detected restricted anomaly in {restrictive_found} iteration")
            break
        elif anomaly_found and i != len(files)-1:
            restrictive_found = i+1
        else:
            restrictive_found = -1
    return (first_found,restrictive_found)

for experiment in ["BERT"]:#,"ChessAlpha","Crypto","DT_A","DT_B","DT_C","DT_D","DT_E","Obs","SimSph","Sort","VoiceCloning"]:
    experiments = os.path.join(home,f"experiments/{experiment}.tar.gz")
    workloads   = get_workloads(experiments)
    defects     = get_defects(experiments)
    print(experiment)
    print("Workloads Detected:",list(workloads))
    print("Defects Detected:",list(defects))
    for workload in workloads:
        for defect in defects:
            print("Start:",workload,defect)
            copy_ref_data(experiments,output,workload,defect,defects)
            run_experiment(experiment,output,workload,defect)
            
